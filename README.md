# Random Quote Generator #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://unruffled-williams-0d52a4.netlify.app/) | [Solution](https://devchallenges.io/solutions/iwURlh35cfc4C4z1rlpg) | [Challenge](https://devchallenges.io/challenges/8Y3J4ucAMQpSnYTwwWW8) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FquoteThumbnail.png%3Falt%3Dmedia%26token%3D156a0ff0-506a-4246-8422-eb1cedab5116&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/8Y3J4ucAMQpSnYTwwWW8) was to build an application to complete the given user stories.

## Built With

- [ReactJS](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Styled Components](https://styled-components.com/)

## Features

This project features a responsive page showcasing a random quote generator, including mobile and desktop views. Built with ReactJS, TypeScript and Styled Components.

## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
