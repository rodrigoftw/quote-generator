import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  :root {
    --white: #FFFFFF;
    --white2: #F2F2F2;
    --black: #4F4F4F;
    --black2: #000000;
    --black3: #333333;
    --yellow: #F7DF94;
    --gray: #828282;
    
    --background: var(--white);
    --header-title-text: var(--black);
    --quote-title-text: var(--black2);
    --quote-border: var(--yellow);
    --author-title-text: var(--black);
    --genre-title-text: var(--gray);
    --footer-title-text: var(--gray);
  }

  #root {
    height: 100vh;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: stretch;
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    @media(max-width: 1080px) {
      font-size: 93.75%;
    }
    
    @media(max-width: 720px) {
      font-size: 86.5%;
    }
  }

  body {
    background: var(--background);
    -webkit-font-smoothing: antialiased;
    font-family: 'Raleway', sans-serif;
    font-weight: 500;
    height: 100vh;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: stretch;
  }

  button {
    font-family: 'Raleway', sans-serif;
  }
  
  input {
    font-family: 'Raleway', sans-serif;
  }
  
  button {
    cursor: pointer;
  }

  footer span, footer span a {
    color: var(--footer-title-text);
  }
  
`;
