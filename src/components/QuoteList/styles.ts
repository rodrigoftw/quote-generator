import styled from 'styled-components';

export const OuterContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;

export const Wrapper = styled.div`
  margin: 3rem 0;
  padding: 2rem 10rem;

  @media(min-width: 320px) and (max-width: 620px) {
    width: 100%;
    margin: 0;
    padding: 2rem;
  }
`;

export const Container = styled.div`
  height: auto;
  width: 100%;
  padding: 0 4rem;
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: flex-start;
  border-left: 8px solid var(--quote-border);

  @media(min-width: 320px) and (max-width: 620px) {
    padding: 0 2rem;
  }
`;

export const QuoteText = styled.h1`
  font-family: 'Raleway', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 36px;
  line-height: 43px;

  color: var(--quote-title-text);

  @media(min-width: 320px) and (max-width: 620px) {
    font-size: 24px;
    line-height: 31px;
  }
`;

export const Author = styled.h4`
  font-family: 'Raleway', sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 42px;

  color: var(--author-title-text);
  margin: 3rem 0 8.75rem 14.5rem;

  @media(min-width: 320px) and (max-width: 620px) {
    margin: 2rem 2rem 1rem 2rem;
    font-size: 32px;
    line-height: 36px;
  }
`;
