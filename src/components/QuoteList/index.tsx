import QuoteLoader from "../QuoteLoader";
import TitleLoader from "../TitleLoader";

import {
  OuterContainer,
  Wrapper,
  Author,
  Container,
  QuoteText
} from "./styles";

interface QuoteProps {
  _id: string;
  quoteText: string;
  quoteAuthor: string;
  quoteGenre: string;
}

interface QuoteListProps {
  author: string;
  quotes: QuoteProps[];
  isLoading: boolean
}

export function QuoteList({ author, quotes, isLoading }: QuoteListProps) {
  return (
    <OuterContainer>
      {isLoading ? (
        <TitleLoader />
      ) : (
        <Author>{author}</Author>
      )}
      {
        isLoading || quotes.length === 0 ? (
          <QuoteLoader />
        ) : (
          quotes.map((quote) => (
            <Wrapper key={quote._id}>
              <Container>
                <QuoteText>{quote.quoteText}</QuoteText>
              </Container>
            </Wrapper>
          ))
        )
      }
    </OuterContainer>
  );
}
