import styled from 'styled-components';

export const OuterContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;

  /* @media(min-width: 320px) and (max-width: 620px) {
    width: 100%;
    max-width: 100%;
    padding: 2rem 2rem 0;
  } */
`;

export const Wrapper = styled.div`
  margin: 3rem 0;
  padding: 2rem 10rem;

  @media(min-width: 320px) and (max-width: 620px) {
    width: 100%;
    margin: 0;
    padding: 2rem;
  }
`;

export const Container = styled.div`
  height: auto;
  width: 100%;
  padding: 0 4rem;
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: flex-start;
  border-left: 8px solid var(--quote-border);

  @media(min-width: 320px) and (max-width: 620px) {
    padding: 0 2rem;
  }
`;

export const QuoteText = styled.h1`
  font-family: 'Raleway', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 36px;
  line-height: 43px;

  color: var(--quote-title-text);

  @media(min-width: 320px) and (max-width: 620px) {
    font-size: 24px;
    line-height: 31px;
  }
`;

export const Author = styled.h4`
  font-family: 'Raleway', sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 28px;

  color: var(--author-title-text);
  margin-bottom: 0.5rem;

  @media(min-width: 320px) and (max-width: 620px) {
    font-size: 20px;
    line-height: 24px;
  }
`;

export const Genre = styled.span`
  font-family: 'Raleway', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;

  color: var(--genre-title-text);
`;

export const QuoteInfoContainer = styled.div`
  height: 9.5rem;
  max-width: 42rem;
  width: 100%;
  margin-left: 12rem;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background: var(--background);
  transition: background 0.2s;
  cursor: pointer;

  div.inner {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    margin-right: auto;
  
    h4, span {
      margin-left: 2.5rem;
    }

    @media(min-width: 320px) and (max-width: 620px) {
      h4, span {
        /* margin-left: 2rem; */
        margin: 0 auto 0 0;
      }
    }
  }

  span.material-icons {
    margin-right: 2.5rem;
    /* margin: 0 2.5rem 0 auto; */
    color: var(--white);

    @media(min-width: 320px) and (max-width: 620px) {
      margin: 0 0 0 auto;
    }
  }

  &:hover {
    background: var(--black3);

    h4, span.material-icons {
     color: var(--white2);
    }
  }

  @media(min-width: 320px) and (max-width: 620px) {
    width: auto;
    margin: 1rem;
    padding: 2rem;
  }
`;
