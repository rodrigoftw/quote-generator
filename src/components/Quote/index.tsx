import QuoteLoader from "../QuoteLoader";
import TitleLoader from "../TitleLoader";
import SubtitleLoader from "../SubtitleLoader";

import {
  OuterContainer,
  Wrapper,
  Container,
  QuoteText,
  Author,
  Genre,
  QuoteInfoContainer
} from "./styles";

interface QuoteProps {
  id?: string;
  text: string;
  author: string;
  genre: string;
  loadMoreQuotes: () => void;
  isLoading: boolean;
}

export function Quote({
  text,
  author,
  genre,
  loadMoreQuotes,
  isLoading
}: QuoteProps) {
  return (
    <OuterContainer>
      <Wrapper>
        <Container>
          {isLoading ? (
            <QuoteLoader />
          ) : (
            <QuoteText>{text}</QuoteText>
          )}
        </Container>
      </Wrapper>

      <QuoteInfoContainer onClick={loadMoreQuotes}>
        <div className="inner">
          {isLoading ? (
            <>
              <TitleLoader />
              <SubtitleLoader />
            </>
          ) : (
            <>
              <Author>{author}</Author>
              <Genre>{genre}</Genre>
            </>
          )}
        </div>
        <span className="material-icons">arrow_right_alt</span>
      </QuoteInfoContainer>
    </OuterContainer>
  );
}
