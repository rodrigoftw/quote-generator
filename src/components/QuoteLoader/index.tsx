import React from "react";
import ContentLoader from "react-content-loader"

const QuoteLoader = ({ ...props }) => {
  return (
    <ContentLoader
      speed={1.8}
      // viewBox="0 0 100% 650"
      // height={"100%"}
      // width={"100%"}
      backgroundColor="#FFFFFF"
      foregroundColor="#E6E6E6"
      style={{ margin: "12px" }}
      {...props}
    >
      <rect x="12" y="0" rx="5" ry="5" width="100%" height="12" />
      <rect x="12" y="25" rx="5" ry="5" width="100%" height="12" />
      <rect x="12" y="50" rx="5" ry="5" width="100%" height="12" />
      <rect x="12" y="75" rx="5" ry="5" width="100%" height="12" />
    </ContentLoader>
  )
}

export default QuoteLoader