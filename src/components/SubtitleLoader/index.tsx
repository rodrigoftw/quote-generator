import React from "react";
import ContentLoader from "react-content-loader"

const SubtitleLoader = ({ ...props }) => {
  return (
    <ContentLoader
      speed={1.8}
      // viewBox="0 0 100% 650"
      // height={"100%"}
      width={"100%"}
      backgroundColor="#FFFFFF"
      foregroundColor="#E6E6E6"
      style={{ margin: "0 12px" }}
      {...props}
    >
      <rect x="0" y="0" rx="5" ry="5" width="100%" height="12" />
    </ContentLoader>
  )
}

export default SubtitleLoader