import styled from 'styled-components';

export const Container = styled.header`
  height: 3rem;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;

  @media(min-width: 320px) and (max-width: 620px) {
    /* width: 100%;
    max-width: 100%;
    padding: 2rem 2rem 0; */
  } 
`;

export const RefreshButton = styled.button`
  font-style: normal;
  font-weight: 500;
  font-size: 1.125rem;
  line-height: 1.375rem;
  margin-right: 6.25rem;

  color: var(--header-title-text);
  background: transparent;
  border: 0;
  height: 100%;
  width: 5.813rem;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  span.material-icons {
    margin-left: 0.25rem;
    font-size: 1.25rem;
  }

  @media(min-width: 320px) and (max-width: 620px) {
    margin-right: 2rem;
  }
`;