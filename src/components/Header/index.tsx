import { Container, RefreshButton } from "./styles";

interface HeaderProps {
  reload: () => void;
}

export function Header({ reload }: HeaderProps) {
  return (
    <Container>
      <RefreshButton onClick={reload}>
        random
        <span className="material-icons">autorenew</span>
      </RefreshButton>
    </Container>
  );
}