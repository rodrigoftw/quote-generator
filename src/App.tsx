import { useEffect, useState } from "react";
import { Header } from "./components/Header";
import { Quote } from "./components/Quote";
import { QuoteList } from "./components/QuoteList";
import { Footer } from "./components/Footer";
import { GlobalStyle } from "./styles/global";

interface QuoteData {
  _id: string;
  quoteText: string;
  quoteAuthor: string;
  quoteGenre: string;
}

interface QuoteProps {
  id: string;
  text: string;
  author: string;
  genre: string;
}

function App() {
  const [loading, setLoading] = useState(false);
  const [toggleShowQuote, setToggleShowQuote] = useState(false);
  const [quotes, setQuotes] = useState<QuoteData[]>([]);
  const [quote, setQuote] = useState<QuoteProps>({
    id: "",
    text: "",
    author: "",
    genre: ""
  });

  const loadQuoteData = () => {
    setLoading(true);
    const url = "https://quote-garden.herokuapp.com/api/v3/quotes/random";
    fetch(url)
      .then(res => res.json())
      .then(res => {
        const newQuote = [...res.data];
        setQuote({
          id: newQuote[0]._id,
          text: newQuote[0].quoteText,
          author: newQuote[0].quoteAuthor,
          genre: newQuote[0].quoteGenre,
        });
        setLoading(false);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  const loadQuotesData = () => {
    setLoading(true);
    const url = `https://quote-garden.herokuapp.com/api/v3/quotes?author=${quote.author}&limit=5`;
    fetch(url)
      .then(res => res.json())
      .then(res => {
        setQuotes(res.data);
        setLoading(false);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  const showQuotesList = () => {
    loadQuotesData();
    setToggleShowQuote(!toggleShowQuote);
  }

  const reload = () => {
    loadQuoteData();
    setToggleShowQuote(false);
  }

  useEffect(() => {
    loadQuoteData();
  }, []);

  return (
    <>
      <Header reload={reload} />
      {toggleShowQuote ? (
        <QuoteList
          author={quote.author}
          quotes={quotes}
          isLoading={loading}
        />
      ) : (
        <Quote
          text={quote.text}
          author={quote.author}
          genre={quote.genre}
          loadMoreQuotes={showQuotesList}
          isLoading={loading}
        />
      )}
      <Footer />
      <GlobalStyle />
    </>
  );
}

export default App;
